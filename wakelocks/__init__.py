from .wakelocks import *

__all__ = [
    'WakeLock',
    'WakeLockError',
    # utils
    'get_available_sleep_methods',
    'get_autosleep_method',
    'set_autosleep_method',
    'is_autosleep_on_',
    'get_all_locks'
]
