from typing import Optional, Literal
from dataclasses import dataclass
from time import time_ns

IO_MODE = Literal['r', 'w']
LOCK_STATE = Literal['locked', 'unlocked']


class WakeLockError(Exception):
    '''
    Custom exception that occurs on errors when reading/writing sysfs
    handlers related to autosleep feature of the kernel.
    '''
    ...


def _io(path: str, mode: IO_MODE, data: Optional[str] = None) -> str:
    try:
        with open(path, mode) as f:
            operation = getattr(f, 'read' if mode == 'r' else 'write')
            result = operation(data)
            if mode == 'r':
                return result
    except Exception as error:
        message = f'{type(error).__name__}: {error.args if hasattr(error, "args") else str(error)}'
        raise WakeLockError(message)


@dataclass
class WakeLock:
    '''
    Note that any space character in `name` gets substituted with "_"
    since there's no spaces allowed in there.
    '''
    name: str
    delay_ns: Optional[int] = None

    def __post_init__(self):
        if not self.name:
            raise WakeLockError('"name" must be a non-zero length string')
        self.name: str = '_'.join(self.name.split())
        self.last_lock_ts = time_ns()

    @property
    def is_expired(self) -> bool:
        return self.delay_ns and time_ns() - self.last_lock_ts > self.delay_ns

    @property
    def state(self) -> LOCK_STATE:
        locks = _io('/sys/power/wake_lock', 'r').split()
        return 'locked' if self.name in locks else 'unlocked'

    @property
    def is_locked(self) -> bool:
        return self.state == 'locked'

    def lock(self) -> None:
        lock_data = self.name + f' {self.delay_ns}' if self.delay_ns else ''
        self.last_lock_ts = time_ns()
        _io(f'/sys/power/wake_lock', 'w', lock_data)

    def unlock(self) -> None:
        if self.is_locked:
            _io('/sys/power/wake_unlock', 'w', self.name)

# Utils


def get_available_sleep_methods() -> list[str]:
    '''
    Returns the list of available metods, e.g. "mem", "disk", etc.
    '''
    methods = _io('/sys/power/state', 'r')
    return methods.split()


def get_autosleep_method() -> str:
    '''
    Returns currently chosen autosleep method.
    '''
    return _io('/sys/power/autosleep', 'r')


def set_autosleep_method(method: str) -> None:
    _io('/sys/power/autosleep', 'w', method)


def is_autosleep_on() -> bool:
    '''
    Checks if autosleep feature is enabled.
    '''
    return 'off' not in get_autosleep_method()


def get_all_locks() -> list[WakeLock]:
    '''
    Returns all existing locks both active and inactive.
    '''
    return list([WakeLock(name) for name in _io(f'/sys/power/{source}', 'r').split()
                 for source in ['wake_lock', 'wake_unlock']])
