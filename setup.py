import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wakelocks",
    version="0.1.0",
    author="nogaems",
    author_email="nomad@ag.ru",
    description="`sysfs` abstraction layer",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url=" https://gitlab.com/nogaems/wakelocks",
    project_urls={
        "Bug Tracker": "https://gitlab.com/nogaems/wakelocks/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    packages=['wakelocks'],
    python_requires=">=3.9",
)
