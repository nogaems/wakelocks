# Wakelocks

Abstract interface for wakelocks on Linux built ontop of [`sysfs` power management interface](https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-power).
